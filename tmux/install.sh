#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"
ln -s tmux.conf.`uname -s` tmux.conf.symlink

case $? in
	0) exit 0;;
	*) exit 1;;
esac
