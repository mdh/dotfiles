#!/bin/sh

curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh

case $? in
  0) exit 0;;
  *) exit 1;;
esac