#!/bin/sh
#
# bootstrap installs things.

DOTFILES_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# set -e

osname=`uname -s`

echo

info ()
{
	printf "  [ \033[00;34m..\033[0m ] $1"
}

user ()
{
	printf "\r  [ \033[0;33m?\033[0m ] $1 "
}

success () {
	printf "\r\033[2K  [ \033[00;32mOK\033[0m ] $1\n"
}

fail () {
  printf "\r\033[2K  [\033[0;31mFAIL\033[0m] $1\n"
  echo
  exit
}

link_files ()
{
	ln -s $1 $2
	success "linked $1 to $2"
}

run_installs ()
{
	info 'running install.sh files'
	echo

	for source in `find -E $DOTFILES_ROOT -maxdepth 2 -type f -perm +111 -regex ".*\/install\.($osname\.)?sh$"`
	do
		user "executing $source"
		echo

		# tput smcup
		printf '\e[7\e[?1049h'


		"$source"
		retv="$?"

		# tput rmcup
		printf '\e[2J\e[?1049l\e8'

		case "$retv" in
			0)	success "installed $source";;
			1)	fail 	"failed $source";;
			*)	info	"unknown return $source";;
		esac
	done
}

install_dotfiles ()
{
	info 'installing dotfiles'

	overwrite_all=false
	backup_all=false
	skip_all=false

	for source in `find -E $DOTFILES_ROOT -maxdepth 2 -regex ".*\.($osname\.)?symlink$"`
	do
		dest="$HOME/.`basename \"${source%.*}\"`"

		if [ -f $dest ] || [ -d $dest ]
		then
		
			overwrite=false
			backup=false
			skip=false
			
			if [ "$overwrite_all" == "false" ] && [ "$backup_all" == "false" ] && [ "$skip_all" == "false" ]
			then
				user "File already exists: `basename $source`, what do you want to do? [s]kip, [S]kip all, [o]verwrite, [O]verwrite all, [b]ackup, [B]ackup all?"
				read -n 1 action
			
				case "$action" in
					o )
					overwrite=true;;
					O )
					overwrite_all=true;;
					b )
					backup=true;;
					B )
					backup_all=true;;
					s )
					skip=true;;
					S )
					skip_all=true;;
					* )
					;;
				esac
			fi
			
			if [ "$overwrite" == "true" ] || [ "$overwrite_all" == "true" ]
			then
				rm -rf $dest
				success "removed $dest"
			fi
			
			if [ "$backup" == "true" ] || [ "$backup_all" == "true" ]
			then
				mv $dest $dest\.backup
				success "moved $dest to $dest.backup"
			fi
			
			if [ "$skip" == "false" ] && [ "$skip_all" == "false" ]
			then
				link_files $source $dest
			else
				success "skipped $source"
			fi
			
		else
			link_files $source $dest
		fi
		
		done
}

# setup_gitconfig
# install_dotfiles

# If we are on a mac, lets install and setup homebrew
# if [ "$(uname -s)" == "Darwin" ]
# then
# info "installing dependencies"
# if . bin/dot > /tmp/dotfiles-dot 2>&1
# then
# success "dependencies installed"
# else
# fail "error installing dependencies"
# fi
# fi

#run_installs
install_dotfiles

echo
echo '  All installed!'
